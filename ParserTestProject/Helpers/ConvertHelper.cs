﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParserTestProject.Helpers
{
    public class ConvertHelper : IConvertHelper
    {
        public TimeSpan? StringToDateTime(string minutes_seconds)
        {
            try
            {
                List<int> vs = minutes_seconds.Split(new char[] { ':' }).Select(x => int.Parse(x)).ToList();

                int hours = vs[0] / 60;
                int minutes = vs[0] % 60;
                int seconds = vs[1];

                return new TimeSpan(hours, minutes, seconds);
            }
            catch { }
            return null;
        }

        public double? CoefToDoubleNullable(string coef)
        {
            try
            {
                coef = coef.Replace('.', ',');
                return double.Parse(coef);
            }
            catch { }
            return null;
        }
    }
}
