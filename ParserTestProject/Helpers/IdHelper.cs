﻿using ParserTestProject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParserTestProject.Helpers
{
    public class IdHelper : IIdHelper
    {
        public int GetNextId(List<FootballMatch> matches)
        {
            if(matches.Count != 0)
            {
                return matches.Last().MatchId + 1;
            }
            return 1;
        }
    }
}
