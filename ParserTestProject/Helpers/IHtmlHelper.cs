﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Helpers
{
    public interface IHtmlHelper
    {
        string GetHtmlPage(string url, EncodingType encodingType = EncodingType.None);
    }
}
