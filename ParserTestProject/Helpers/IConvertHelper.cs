﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Helpers
{
    public interface IConvertHelper
    {
        TimeSpan? StringToDateTime(string minutes_seconds);
        double? CoefToDoubleNullable(string coef);
    }
}
