﻿using ParserTestProject.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Helpers
{
    public interface IIdHelper
    {
        int GetNextId(List<FootballMatch> matches);
    }
}
