﻿using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;
using ParserTestProject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParserTestProject.Helpers
{
    public class ParserHelper : IParserHelper
    {
        private readonly IConvertHelper _convertHelper;
        private readonly IHtmlHelper _htmlHelper;
        private readonly IIdHelper _idHelper;

        public ParserHelper(IConvertHelper convertHelper, IHtmlHelper htmlHelper, IIdHelper idHelper)
        {
            _convertHelper = convertHelper;
            _htmlHelper = htmlHelper;
            _idHelper = idHelper;
        }


        public List<FootballMatch> GetFootballMatches()
        {
            List<FootballMatch> matches = new List<FootballMatch> { };
            FootballMatch tempMatch;

            HtmlParser parser = new HtmlParser();
            IHtmlDocument document = parser.ParseDocument(_htmlHelper.GetHtmlPage("https://ua1xbet.com/ua/live/Football/"));

            IEnumerable<IElement> matchElements;
            IEnumerable<IElement> teamElements;
            IEnumerable<IElement> coefElements;

            IElement urlElement;

            matchElements = document.QuerySelectorAll("div[class='c-events__item c-events__item_game c-events-scoreboard__wrap']");

            foreach (IElement matchElement in matchElements)
            {
                urlElement = matchElement.QuerySelector("a[class='c-events__name']");
                teamElements = matchElement.QuerySelectorAll("span[class='c-events__team']");
                coefElements = matchElement.QuerySelectorAll("div[class='c-bets'] > a");

                tempMatch = new FootballMatch { };

                tempMatch.MatchId = _idHelper.GetNextId(matches);
                tempMatch.Url = String.Format("https://1xbet.com/{0}", urlElement.GetAttribute("href"));

                try
                {
                    tempMatch.CurrentTime = _convertHelper.StringToDateTime(matchElement.QuerySelectorAll("div[class='c-events__time  '] > span").First().TextContent);
                }
                catch
                {
                    tempMatch.CurrentTime = null;
                }

                tempMatch.FirstTeam = new Team
                {
                    Name = teamElements.First().TextContent
                };
                tempMatch.SecondTeam = new Team
                {
                    Name = teamElements.Skip(1).First().TextContent
                };

                tempMatch.FirstTeamWinCoef = _convertHelper.CoefToDoubleNullable(coefElements.First().TextContent);
                tempMatch.DrawCoef = _convertHelper.CoefToDoubleNullable(coefElements.Skip(1).First().TextContent);
                tempMatch.SecondTeamWinCoef = _convertHelper.CoefToDoubleNullable(coefElements.Skip(2).First().TextContent);

                ParseFootballMatchScore(tempMatch, matchElement);

                matches.Add(tempMatch);
            }

            return matches;
        }

        private void ParseFootballMatchScore(FootballMatch tempMatch, IElement matchElement)
        {

            if (matchElement.QuerySelectorAll("div[class='c-penalty football']").Count() == 0)
            {
                if (matchElement.QuerySelectorAll("span[class='c-events__teams'] > span:contains('Екстра-тайм')").Count() != 0)
                {
                    ParseExtraTimeScore(tempMatch, matchElement);
                }
                else
                {
                    ParseStandartScore(tempMatch, matchElement);
                }
            }
            else
            {
                ParsePenaltyScore(tempMatch, matchElement);
            }
        }

        private void ParseExtraTimeScore(FootballMatch tempMatch, IElement matchElement)
        {
            IElement tempElement;
            IEnumerable<IElement> scopeElements = matchElement.QuerySelectorAll("div[class='c-events-scoreboard__line']");

            tempMatch.FootballMatchScore = new FootballMatchExtraTimeScore { };

            tempElement = scopeElements.First().QuerySelector("span[class='c-events-scoreboard__cell c-events-scoreboard__cell--all']");
            ((FootballMatchExtraTimeScore)tempMatch.FootballMatchScore).FirstTeamExtraTimeScore = int.Parse(tempElement.TextContent);
            tempElement = scopeElements.Skip(1).First().QuerySelector("span[class='c-events-scoreboard__cell c-events-scoreboard__cell--all']");
            ((FootballMatchExtraTimeScore)tempMatch.FootballMatchScore).SecondTeamExtraTimeScore = int.Parse(tempElement.TextContent);
        }

        private void ParseStandartScore(FootballMatch tempMatch, IElement matchElement)
        {
            IEnumerable<IElement> tempElements;
            IEnumerable<IElement> scopeElements = matchElement.QuerySelectorAll("div[class='c-events-scoreboard__line']");

            tempMatch.FootballMatchScore = new FootballMatchStandartScore { };

            tempElements = scopeElements.First().QuerySelectorAll("span[class='c-events-scoreboard__cell']");
            ((FootballMatchStandartScore)tempMatch.FootballMatchScore).FirstTeamFirstHalfScore = int.Parse(tempElements.First().TextContent);

            if (tempElements.Count() == 1)
            {
                ((FootballMatchStandartScore)tempMatch.FootballMatchScore).FirstTeamSecondHalfScore = 0;
            }
            else
            {
                ((FootballMatchStandartScore)tempMatch.FootballMatchScore).FirstTeamSecondHalfScore = int.Parse(tempElements.Skip(1).First().TextContent);
            }

            tempElements = scopeElements.Skip(1).First().QuerySelectorAll("span[class='c-events-scoreboard__cell']");
            ((FootballMatchStandartScore)tempMatch.FootballMatchScore).SecondTeamFirstHalfScore = int.Parse(tempElements.First().TextContent);

            if (tempElements.Count() == 1)
            {
                ((FootballMatchStandartScore)tempMatch.FootballMatchScore).SecondTeamSecondHalfScore = 0;
            }
            else
            {
                ((FootballMatchStandartScore)tempMatch.FootballMatchScore).SecondTeamSecondHalfScore = int.Parse(tempElements.Skip(1).First().TextContent);
            }
        }

        private void ParsePenaltyScore(FootballMatch tempMatch, IElement matchElement)
        {
            IElement tempElement;

            tempMatch.FootballMatchScore = new FootballMatchPenaltyScore { };

            tempElement = matchElement.QuerySelectorAll("div[class='c-penalty football']").First();

            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).FirstTeamWinPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--win']").Count();
            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).FirstTeamMissingPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--missing']").Count();
            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).FirstTeamWaitPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--wait']").Count();

            tempElement = matchElement.QuerySelectorAll("div[class='c-penalty football']").Skip(1).First();

            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).SecondTeamWinPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--win']").Count();
            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).SecondTeamMissingPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--missing']").Count();
            ((FootballMatchPenaltyScore)tempMatch.FootballMatchScore).SecondTeamWaitPenalty = tempElement.QuerySelectorAll("div[class='c-penalty__item c-penalty__item--wait']").Count();
        }
    }
}
