﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace ParserTestProject.Helpers
{
    public enum EncodingType
    {
        None = 0, Default = 1
    }

    public class HtmlHelper : IHtmlHelper
    {
        public string GetHtmlPage(string url, EncodingType encodingType = EncodingType.None)
        {
            string HtmlText = String.Empty;

            for (int i = 0; i < 20; i++)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.AutomaticDecompression = DecompressionMethods.GZip;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream responseStream = response.GetResponseStream())             
                    using (StreamReader streamReader = new StreamReader(responseStream))     
                    {
                        //HtmlText = File.ReadAllText(@"C:/Users/Home PC/Desktop/test.html"); 
                        HtmlText = streamReader.ReadToEnd();
                    }
                }
                catch
                {
                    Thread.Sleep(100);
                    continue;
                }
                break;
            }
            return HtmlText;
        }
    }
}
