﻿using ParserTestProject.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Helpers
{
    public interface IParserHelper
    {
        List<FootballMatch> GetFootballMatches();
    }
}
