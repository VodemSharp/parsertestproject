﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class Match
    {
        public int MatchId { get; set; }

        public string Url { get; set; }
        public TimeSpan? CurrentTime { get; set; }

        public virtual void Print()
        {
            Console.WriteLine("Match #{0}", MatchId);
            Console.WriteLine(" Url: {0}", Url);
            if (CurrentTime.HasValue)
            {
                Console.WriteLine(" Current time: {0}:{1}", CurrentTime.Value.Hours * 60 + CurrentTime.Value.Minutes, CurrentTime.Value.Seconds);
            }
            else
            {
                Console.WriteLine(" Current time: 00:00");
            }
        }
    }
}
