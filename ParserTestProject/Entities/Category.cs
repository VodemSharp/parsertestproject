﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }
        public List<Match> Matches { get; set; }
    }
}
