﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class FootballMatchExtraTimeScore : IFootballMatchScore
    {
        public int FirstTeamExtraTimeScore { get; set; }
        public int SecondTeamExtraTimeScore { get; set; }

        public void Print(TimeSpan? currentTime)
        {
            Console.WriteLine(" Extra time: {0}:{1}", FirstTeamExtraTimeScore, SecondTeamExtraTimeScore);
        }
    }
}
