﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class FootballMatchStandartScore : IFootballMatchScore
    {
        public int FirstTeamFirstHalfScore { get; set; }
        public int FirstTeamSecondHalfScore { get; set; }

        public int SecondTeamFirstHalfScore { get; set; }
        public int SecondTeamSecondHalfScore { get; set; }

        public void Print(TimeSpan? currentTime)
        {
            Console.WriteLine(" Scope: ");
            Console.WriteLine("  First half: {0}:{1}", FirstTeamFirstHalfScore, SecondTeamFirstHalfScore);
            if (currentTime.HasValue)
            {
                if (currentTime.Value.TotalMinutes >= 45)
                {
                    Console.WriteLine("  Second half: {0}:{1}", FirstTeamSecondHalfScore, SecondTeamSecondHalfScore);
                }
            }
            else
            {
                Console.WriteLine("  Second half: {0}:{1}", FirstTeamSecondHalfScore, SecondTeamSecondHalfScore);
            }
            Console.WriteLine("  Total scope: {0}:{1}", FirstTeamFirstHalfScore + FirstTeamSecondHalfScore, SecondTeamFirstHalfScore + SecondTeamSecondHalfScore);
        }
    }
}
