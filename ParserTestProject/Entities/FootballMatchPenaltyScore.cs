﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class FootballMatchPenaltyScore : IFootballMatchScore
    {
        public int FirstTeamWinPenalty { get; set; }
        public int SecondTeamWinPenalty { get; set; }
        public int FirstTeamMissingPenalty { get; set; }
        public int SecondTeamMissingPenalty { get; set; }
        public int FirstTeamWaitPenalty { get; set; }
        public int SecondTeamWaitPenalty { get; set; }

        public void Print(TimeSpan? currentTime)
        {
            Console.WriteLine(" Penalty: ");
            Console.WriteLine("  Win penalty: {0}:{1}", FirstTeamWinPenalty, SecondTeamWinPenalty);
            Console.WriteLine("  Missing penalty: {0}:{1}", FirstTeamMissingPenalty, SecondTeamMissingPenalty);
            Console.WriteLine("  Wait penalty: {0}:{1}", FirstTeamWaitPenalty, SecondTeamWaitPenalty);
        }
    }
}
