﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public interface IFootballMatchScore {
        void Print(TimeSpan? currentTime);
    }
}
