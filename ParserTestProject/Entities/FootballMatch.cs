﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParserTestProject.Entities
{
    public class FootballMatch : Match
    {
        public Team FirstTeam { get; set; }
        public Team SecondTeam { get; set; }

        public double? FirstTeamWinCoef { get; set; }
        public double? SecondTeamWinCoef { get; set; }
        public double? DrawCoef { get; set; }

        public IFootballMatchScore FootballMatchScore { get; set; }

        public override void Print()
        {
            base.Print();
            Console.WriteLine(" Team #1: {0}", FirstTeam.Name);
            Console.WriteLine(" Team #2: {0}", SecondTeam.Name);

            FootballMatchScore.Print(CurrentTime);
        }

        public void PrintCoefs()
        {
            Console.WriteLine("Coefs of match #{0}", MatchId);
            Console.WriteLine(" Team #1 win coef: {0}", FirstTeamWinCoef == null ? "-" : FirstTeamWinCoef.ToString());
            Console.WriteLine(" Draw coef: {0}", DrawCoef == null ? "-" : DrawCoef.ToString());
            Console.WriteLine(" Team #2 win coef: {0}", SecondTeamWinCoef == null ? "-" : SecondTeamWinCoef.ToString());
        }
    }
}
