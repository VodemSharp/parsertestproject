﻿using Microsoft.Extensions.DependencyInjection;
using ParserTestProject.Entities;
using ParserTestProject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParserTestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<IHtmlHelper, HtmlHelper>()
                .AddTransient<IConvertHelper, ConvertHelper>()
                .AddTransient<IIdHelper, IdHelper>()
                .AddTransient<IParserHelper, ParserHelper>()
                .BuildServiceProvider();

            List<FootballMatch> footballMatches = serviceProvider.GetService<IParserHelper>().GetFootballMatches();

            foreach(FootballMatch footballMatch in footballMatches)
            {
                footballMatch.Print();
                Console.WriteLine();
            }

            int tempId;

            for(; ; )
            {
                try
                {
                    tempId = int.Parse(Console.ReadLine());
                    footballMatches.First(f => f.MatchId == tempId).PrintCoefs();
                    Console.WriteLine();
                }
                catch
                {
                    Console.WriteLine("Incorrect id!");
                }
            }
        }
    }
}
